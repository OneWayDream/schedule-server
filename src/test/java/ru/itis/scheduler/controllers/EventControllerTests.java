package ru.itis.scheduler.controllers;

import com.google.gson.*;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import ru.itis.scheduler.entities.Interval;
import ru.itis.scheduler.entities.dtos.EventDto;
import ru.itis.scheduler.entities.forms.MultipleAccountsEventForm;
import ru.itis.scheduler.entities.forms.SingleAccountEventForm;
import ru.itis.scheduler.exceptions.persistence.EntityNotExistsException;
import ru.itis.scheduler.exceptions.persistence.EventDateOverlapException;
import ru.itis.scheduler.services.EventService;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@DisplayName("EventController is working when")
public class EventControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EventService eventService;

    private final Gson gson = new GsonBuilder()
            .registerTypeAdapter(LocalDate.class, (JsonDeserializer<LocalDate>)
                    (json, type, jsonDeserializationContext) -> LocalDate.parse(json.getAsString())
            )
            .registerTypeAdapter(LocalDateTime.class, (JsonDeserializer<LocalDateTime>)
                    (json, type, jsonDeserializationContext) -> LocalDateTime.parse(json.getAsString())
            )
            .registerTypeAdapter(LocalDate.class, (JsonSerializer<LocalDate>)
                    (localDate, srcType, context) -> new JsonPrimitive(localDate.toString())
            )
            .registerTypeAdapter(LocalDateTime.class, (JsonSerializer<LocalDateTime>)
                    (localDateTime, srcType, context) -> new JsonPrimitive(localDateTime.toString())
            )
            .create();

    private static final LocalDateTime secondsNow = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS);
    private static final LocalDateTime now = secondsNow.truncatedTo(ChronoUnit.MINUTES);

    @BeforeEach
    public void setUp(){
        when(eventService.addEvent(AddEventForSingleAccountTests.getValidEventDto()))
                .thenReturn(AddEventForSingleAccountTests.getAddedValidEventDto());
        when(eventService.addEvent(AddEventForSingleAccountTests.getNotExistingAccountEventDto()))
                .thenThrow(new EntityNotExistsException(
                        AddEventForSingleAccountTests.getNotExistingAccountIdExceptionMessage()
                ));
        when(eventService.addEvent(AddEventForSingleAccountTests.getOverlapDateEventDto()))
                .thenThrow(new EventDateOverlapException(
                        AddEventForSingleAccountTests.getOverlapDateExceptionMessage()
                ));

        when(eventService.addEvent(AddEventForMultipleAccountsTests.getValidAccountIds(),
                AddEventForMultipleAccountsTests.getValidEventDto()))
                .thenReturn(AddEventForMultipleAccountsTests.getAddedValidEventDtos());
        when(eventService.addEvent(AddEventForMultipleAccountsTests.getNotExistingAccountIds(),
                AddEventForMultipleAccountsTests.getNotExistingAccountEventDto()))
                .thenThrow(new EntityNotExistsException(
                        AddEventForMultipleAccountsTests.getNotExistingAccountIdExceptionMessage()
                ));
        when(eventService.addEvent(AddEventForMultipleAccountsTests.getEmptyAccountIds(),
                AddEventForMultipleAccountsTests.getEmptyAccountIdsEventDto()))
                .thenReturn(AddEventForMultipleAccountsTests.getAddedEmptyAccountIdsEventDtos());
        when(eventService.addEvent(AddEventForMultipleAccountsTests.getValidAccountIds(),
                AddEventForMultipleAccountsTests.getOverlapAccountEventDto()))
                .thenThrow(new EventDateOverlapException(
                        AddEventForMultipleAccountsTests.getOverlapDateExceptionMessage()
                ));

        when(eventService.getCommonFreeIntervals(GetFreeTimeIntervalsTests.getValidAccountIds()))
                .thenReturn(GetFreeTimeIntervalsTests.getValidResult());
    }

    @Nested
    @DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
    @DisplayName("addEventForSingleAccount() in working when")
    public class AddEventForSingleAccountTests{

        @Test
        public void add_valid_event() throws Exception{
            mockMvc.perform(
                            post("/event")
                                    .contentType("application/json")
                                    .content(getValidSingleAccountEventFormContent())
                    )
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id", is(getAddedValidEventDto().getId().intValue())));
        }

        private String getValidSingleAccountEventFormContent(){
            return gson.toJson(getValidSingleAccountEventForm());
        }

        private static SingleAccountEventForm getValidSingleAccountEventForm(){
            return SingleAccountEventForm.builder()
                    .accountId(getValidAccountId())
                    .startTime(now)
                    .endTime(now.plusMinutes(10))
                    .description("Description")
                    .build();
        }

        public static Long getValidAccountId(){
            return 1L;
        }

        public static EventDto getValidEventDto(){
            SingleAccountEventForm form = getValidSingleAccountEventForm();
            return EventDto.builder()
                    .accountId(getValidAccountId())
                    .startTime(form.getStartTime())
                    .endTime(form.getEndTime())
                    .description(form.getDescription())
                    .build();
        }

        public static EventDto getAddedValidEventDto(){
            EventDto event = getValidEventDto();
            return EventDto.builder()
                    .id(1L)
                    .accountId(event.getAccountId())
                    .startTime(event.getStartTime())
                    .endTime(event.getEndTime())
                    .description(event.getDescription())
                    .build();
        }

        @Test
        public void add_event_for_not_existing_account() throws Exception {
            mockMvc.perform(
                            post("/event")
                                    .contentType("application/json")
                                    .content(getNotExistingAccountSingleAccountEventFormContent())
                    )
                    .andDo(print())
                    .andExpect(status().is(404));
        }

        private String getNotExistingAccountSingleAccountEventFormContent() {
            return gson.toJson(getNotExistingAccountSingleAccountEventForm());
        }

        private static SingleAccountEventForm getNotExistingAccountSingleAccountEventForm(){
            return SingleAccountEventForm.builder()
                    .accountId(getNotExistingAccountId())
                    .startTime(now)
                    .endTime(now.plusMinutes(10))
                    .description("Description")
                    .build();
        }

        public static EventDto getNotExistingAccountEventDto(){
            SingleAccountEventForm form = getValidSingleAccountEventForm();
            return EventDto.builder()
                    .accountId(getNotExistingAccountId())
                    .startTime(form.getStartTime())
                    .endTime(form.getEndTime())
                    .description(form.getDescription())
                    .build();
        }

        public static Long getNotExistingAccountId(){
            return 2L;
        }

        public static String getNotExistingAccountIdExceptionMessage(){
            return "account with id = 2";
        }

        @Test
        public void add_invalid_date_event() throws Exception {
            mockMvc.perform(
                            post("/event")
                                    .contentType("application/json")
                                    .content(getInvalidDateSingleAccountEventFormContent())
                    )
                    .andDo(print())
                    .andExpect(status().is(400));
        }

        private String getInvalidDateSingleAccountEventFormContent() {
            return gson.toJson(getInvalidDateSingleAccountEventForm());
        }

        private static SingleAccountEventForm getInvalidDateSingleAccountEventForm(){
            return SingleAccountEventForm.builder()
                    .accountId(getValidAccountId())
                    .startTime(now.plusMinutes(10))
                    .endTime(now)
                    .description("Description")
                    .build();
        }

        @Test
        public void add_overlap_event() throws Exception {
            mockMvc.perform(
                            post("/event")
                                    .contentType("application/json")
                                    .content(getOverlapDateSingleAccountEventFormContent())
                    )
                    .andDo(print())
                    .andExpect(status().is(453));
        }

        private String getOverlapDateSingleAccountEventFormContent() {
            return gson.toJson(getOverlapDateSingleAccountEventForm());
        }

        private static SingleAccountEventForm getOverlapDateSingleAccountEventForm(){
            return SingleAccountEventForm.builder()
                    .accountId(getValidAccountId())
                    .startTime(now.plusMinutes(5))
                    .endTime(now.plusMinutes(10))
                    .description("Description")
                    .build();
        }

        public static EventDto getOverlapDateEventDto(){
            SingleAccountEventForm form = getOverlapDateSingleAccountEventForm();
            return EventDto.builder()
                    .accountId(getValidAccountId())
                    .startTime(form.getStartTime())
                    .endTime(form.getEndTime())
                    .description(form.getDescription())
                    .build();
        }

        public static String getOverlapDateExceptionMessage(){
            return "" + getOverlapDateSingleAccountEventForm().getAccountId();
        }

    }

    @Nested
    @DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
    @DisplayName("addEventForMultipleAccounts() in working when")
    public class AddEventForMultipleAccountsTests{

        @Test
        public void add_valid_event() throws Exception {
            List<EventDto> expected = getAddedValidEventDtos();
            mockMvc.perform(
                            post("/event/multiple")
                                    .contentType("application/json")
                                    .content(getValidMultipleAccountsEventFormContent())
                    )
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$[0].id", is(expected.get(0).getId().intValue())))
                    .andExpect(jsonPath("$[1].id", is(expected.get(1).getId().intValue())));
        }

        private String getValidMultipleAccountsEventFormContent() {
            return gson.toJson(getValidMultiplesAccountEventForm());
        }

        private static MultipleAccountsEventForm getValidMultiplesAccountEventForm(){
            return MultipleAccountsEventForm.builder()
                    .accountIds(getValidAccountIds())
                    .startTime(now)
                    .endTime(now.plusMinutes(10))
                    .description("Description")
                    .build();
        }

        public static List<Long> getValidAccountIds(){
            return List.of(1L, 2L);
        }

        public static EventDto getValidEventDto(){
            MultipleAccountsEventForm form = getValidMultiplesAccountEventForm();
            return EventDto.builder()
                    .startTime(form.getStartTime())
                    .endTime(form.getEndTime())
                    .description(form.getDescription())
                    .build();
        }

        public static List<EventDto> getAddedValidEventDtos(){
            EventDto event = getValidEventDto();
            List<EventDto> result = new ArrayList<>();
            for (Long id: getValidAccountIds()){
                result.add(EventDto.builder()
                        .id(id)
                        .accountId(id)
                        .startTime(event.getStartTime())
                        .endTime(event.getEndTime())
                        .description(event.getDescription())
                        .build());
            }
            return result;
        }

        @Test
        public void add_event_for_not_existing_account() throws Exception {
            mockMvc.perform(
                            post("/event/multiple")
                                    .contentType("application/json")
                                    .content(getNotExistingAccountMultipleAccountsEventFormContent())
                    )
                    .andDo(print())
                    .andExpect(status().is(404));
        }

        private String getNotExistingAccountMultipleAccountsEventFormContent() {
            return gson.toJson(getNotExistingAccountMultipleAccountsEventForm());
        }

        private static MultipleAccountsEventForm getNotExistingAccountMultipleAccountsEventForm(){
            return MultipleAccountsEventForm.builder()
                    .accountIds(getNotExistingAccountIds())
                    .startTime(now)
                    .endTime(now.plusMinutes(10))
                    .description("Description")
                    .build();
        }

        public static EventDto getNotExistingAccountEventDto(){
            MultipleAccountsEventForm form = getNotExistingAccountMultipleAccountsEventForm();
            return EventDto.builder()
                    .startTime(form.getStartTime())
                    .endTime(form.getEndTime())
                    .description(form.getDescription())
                    .build();
        }

        public static List<Long> getNotExistingAccountIds(){
            return List.of(2L, 3L);
        }

        public static String getNotExistingAccountIdExceptionMessage(){
            return "account with id = 3";
        }

        @Test
        public void add_event_for_empty_account_ids_list() throws Exception {
            mockMvc.perform(
                            post("/event/multiple")
                                    .contentType("application/json")
                                    .content(getEmptyAccountIdsMultipleAccountsEventFormContent())
                    )
                    .andDo(print())
                    .andExpect(status().is(200));
        }

        private String getEmptyAccountIdsMultipleAccountsEventFormContent() {
            return gson.toJson(getEmptyAccountIdsMultipleAccountsEventForm());
        }

        private static MultipleAccountsEventForm getEmptyAccountIdsMultipleAccountsEventForm(){
            return MultipleAccountsEventForm.builder()
                    .accountIds(getEmptyAccountIds())
                    .startTime(now)
                    .endTime(now.plusMinutes(10))
                    .description("Description")
                    .build();
        }

        public static EventDto getEmptyAccountIdsEventDto(){
            MultipleAccountsEventForm form = getEmptyAccountIdsMultipleAccountsEventForm();
            return EventDto.builder()
                    .startTime(form.getStartTime())
                    .endTime(form.getEndTime())
                    .description(form.getDescription())
                    .build();
        }

        public static List<Long> getEmptyAccountIds(){
            return List.of();
        }

        public static List<EventDto> getAddedEmptyAccountIdsEventDtos(){
            return new ArrayList<>();
        }

        @Test
        public void add_invalid_date_event() throws Exception {
            mockMvc.perform(
                            post("/event/multiple")
                                    .contentType("application/json")
                                    .content(getInvalidDateMultipleAccountsEventFormContent())
                    )
                    .andDo(print())
                    .andExpect(status().is(400));
        }

        private String getInvalidDateMultipleAccountsEventFormContent() {
            return gson.toJson(getInvalidDateMultipleAccountsEventForm());
        }

        private static MultipleAccountsEventForm getInvalidDateMultipleAccountsEventForm(){
            return MultipleAccountsEventForm.builder()
                    .accountIds(getValidAccountIds())
                    .startTime(now.plusMinutes(10))
                    .endTime(now)
                    .description("Description")
                    .build();
        }

        @Test
        public void add_overlap_event() throws Exception {
            mockMvc.perform(
                            post("/event/multiple")
                                    .contentType("application/json")
                                    .content(getOverlapMultipleAccountsEventFormContent())
                    )
                    .andDo(print())
                    .andExpect(status().is(453));
        }

        private String getOverlapMultipleAccountsEventFormContent() {
            return gson.toJson(getOverlapMultipleAccountsEventForm());
        }

        private static MultipleAccountsEventForm getOverlapMultipleAccountsEventForm(){
            return MultipleAccountsEventForm.builder()
                    .accountIds(getValidAccountIds())
                    .startTime(now.plusMinutes(5))
                    .endTime(now.plusMinutes(10))
                    .description("Description")
                    .build();
        }

        public static EventDto getOverlapAccountEventDto(){
            MultipleAccountsEventForm form = getOverlapMultipleAccountsEventForm();
            return EventDto.builder()
                    .startTime(form.getStartTime())
                    .endTime(form.getEndTime())
                    .description(form.getDescription())
                    .build();
        }

        public static String getOverlapDateExceptionMessage(){
            return "" + getOverlapMultipleAccountsEventForm().getAccountIds().get(0);
        }

    }

    @Nested
    @DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
    @DisplayName("GetFreeTimeIntervals() in working when")
    public class GetFreeTimeIntervalsTests{

        @Test
        public void execute_valid_request() throws Exception {
            List<Interval> expected = getValidResult();
            mockMvc.perform(
                            get("/event/free-intervals")
                                    .param("account_ids", getValidAccountIdsString())
                    )
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$[0].start", is(expected.get(0).getStart().toString())))
                    .andExpect(jsonPath("$[0].end", is(expected.get(0).getEnd().toString())));
        }

        public static String getValidAccountIdsString(){
            return "1,2";
        }

        public static List<Long> getValidAccountIds(){
            return List.of(1L, 2L);
        }

        public static List<Interval> getValidResult(){
            return List.of(new Interval(secondsNow.plusMinutes(2), secondsNow.plusMinutes(12)));
        }

        @Test
        public void execute_invalid_parameter_requests() throws Exception {
            mockMvc.perform(
                            get("/event/free-intervals")
                                    .param("account_ids", getInvalidAccountIdsString())
                    )
                    .andDo(print())
                    .andExpect(status().is(400));
        }

        public static String getInvalidAccountIdsString(){
            return "a,b";
        }

        @Test
        public void execute_empty_ids_requests() throws Exception {
            mockMvc.perform(
                            get("/event/free-intervals")
                                    .param("account_ids", getEmptyAccountIdsString())
                    )
                    .andDo(print())
                    .andExpect(status().is(400));
        }

        public static String getEmptyAccountIdsString(){
            return "";
        }

    }

}
