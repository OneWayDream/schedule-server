package ru.itis.scheduler.controllers;

import com.google.gson.Gson;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import ru.itis.scheduler.entities.forms.NewAccountForm;
import ru.itis.scheduler.exceptions.persistence.LoginAlreadyInUseException;
import ru.itis.scheduler.models.Account;
import ru.itis.scheduler.services.AccountService;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@DisplayName("AccountController is working when")
public class AccountControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AccountService accountService;

    private final Gson gson = new Gson();

    @BeforeEach
    public void setUp(){
        when(accountService.add(CreateAccountTests.getValidAccount()))
                .thenReturn(CreateAccountTests.getRegisteredAccount());
        when(accountService.add(CreateAccountTests.getExistingLoginAccount()))
                .thenThrow(new LoginAlreadyInUseException());
    }

    @Nested
    @DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
    @DisplayName("createAccount() in working when")
    public class CreateAccountTests {

        @Test
        public void add_valid_account() throws Exception {
            mockMvc.perform(
                            post("/account")
                                    .contentType("application/json")
                                    .content(getValidAccountContent())
                    )
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id", is(getRegisteredAccount().getId().intValue())));
        }

        private String getValidAccountContent(){
            return gson.toJson(getValidAccountForm());
        }

        private static NewAccountForm getValidAccountForm(){
            return NewAccountForm.builder()
                    .login("OneWayDream")
                    .name("Kirill")
                    .build();
        }

        public static Account getValidAccount(){
            NewAccountForm form = getValidAccountForm();
            return Account.builder()
                    .login(form.getLogin())
                    .name(form.getName())
                    .build();
        }

        public static Account getRegisteredAccount(){
            Account validAccount = getValidAccount();
            validAccount.setId(1L);
            return validAccount;
        }

        @Test
        public void add_account_with_existing_login() throws Exception {
            mockMvc.perform(
                            post("/account")
                                    .contentType("application/json")
                                    .content(getExistingLoginAccountContent())
                    )
                    .andDo(print())
                    .andExpect(status().is(452));
        }

        private String getExistingLoginAccountContent(){
            return gson.toJson(getExistingLoginAccountForm());
        }

        private static NewAccountForm getExistingLoginAccountForm(){
            return NewAccountForm.builder()
                    .login("ExistingLogin")
                    .name("Kirill")
                    .build();
        }

        public static Account getExistingLoginAccount(){
            NewAccountForm form = getExistingLoginAccountForm();
            return Account.builder()
                    .login(form.getLogin())
                    .name(form.getName())
                    .build();
        }

        @Test
        public void add_account_with_blank_login() throws Exception {
            mockMvc.perform(
                            post("/account")
                                    .contentType("application/json")
                                    .content(getBlankLoginAccountContent())
                    )
                    .andDo(print())
                    .andExpect(status().is(400));
        }

        private String getBlankLoginAccountContent(){
            return gson.toJson(getBlankLoginAccountForm());
        }

        private static NewAccountForm getBlankLoginAccountForm(){
            return NewAccountForm.builder()
                    .login(null)
                    .name("Kirill")
                    .build();
        }

        @Test
        public void add_account_with_blank_name() throws Exception {
            mockMvc.perform(
                            post("/account")
                                    .contentType("application/json")
                                    .content(getBlankNameAccountContent())
                    )
                    .andDo(print())
                    .andExpect(status().is(400));
        }

        private String getBlankNameAccountContent(){
            return gson.toJson(getBlankNameAccountForm());
        }

        private static NewAccountForm getBlankNameAccountForm(){
            return NewAccountForm.builder()
                    .login("OneWayDream")
                    .name(null)
                    .build();
        }

        @Test
        public void add_account_with_invalid_login() throws Exception {
            mockMvc.perform(
                            post("/account")
                                    .contentType("application/json")
                                    .content(getInvalidLoginAccountContent())
                    )
                    .andDo(print())
                    .andExpect(status().is(400));
        }

        private String getInvalidLoginAccountContent(){
            return gson.toJson(getInvalidLoginAccountForm());
        }

        private static NewAccountForm getInvalidLoginAccountForm(){
            return NewAccountForm.builder()
                    .login("")
                    .name("Kirill")
                    .build();
        }

        @Test
        public void add_account_with_invalid_name() throws Exception {
            mockMvc.perform(
                            post("/account")
                                    .contentType("application/json")
                                    .content(getInvalidNameAccountContent())
                    )
                    .andDo(print())
                    .andExpect(status().is(400));
        }

        private String getInvalidNameAccountContent(){
            return gson.toJson(getInvalidNameAccountForm());
        }

        private static NewAccountForm getInvalidNameAccountForm(){
            return NewAccountForm.builder()
                    .login("OneWayDream")
                    .name("")
                    .build();
        }

    }

}
