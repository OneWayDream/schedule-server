package ru.itis.scheduler.exceptions.persistence;

import jakarta.persistence.PersistenceException;

public class EventDateOverlapException extends PersistenceException {

    public EventDateOverlapException(String message) {
        super(message);
    }
}
