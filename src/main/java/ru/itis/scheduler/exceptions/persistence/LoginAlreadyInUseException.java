package ru.itis.scheduler.exceptions.persistence;

import jakarta.persistence.PersistenceException;

public class LoginAlreadyInUseException extends PersistenceException {

    public LoginAlreadyInUseException() {
        super();
    }

}
