package ru.itis.scheduler.exceptions.persistence;

import jakarta.persistence.PersistenceException;

public class EntityNotExistsException extends PersistenceException {

    public EntityNotExistsException(String message) {
        super(message);
    }
}
