package ru.itis.scheduler.exceptions.handlers;

import jakarta.persistence.PersistenceException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ru.itis.scheduler.exceptions.intervals.EmptyAccountIdsException;
import ru.itis.scheduler.exceptions.intervals.InvalidAccountIdsException;
import ru.itis.scheduler.exceptions.persistence.EntityNotExistsException;
import ru.itis.scheduler.exceptions.persistence.EventDateOverlapException;
import ru.itis.scheduler.exceptions.persistence.LoginAlreadyInUseException;

@ControllerAdvice
public class GlobalExceptionsHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(PersistenceException.class)
    public ResponseEntity<?> handlePersistenceException(PersistenceException exception){
        if (exception instanceof LoginAlreadyInUseException){
            return ResponseEntity.status(HttpErrorStatus.EXISTING_LOGIN.value()).body("This login is already exists.");
        }
        else if (exception instanceof EntityNotExistsException){
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body("Entity doesn't exist: " + (exception.getMessage() != null ? exception.getMessage() : ""));
        }
        else if (exception instanceof EventDateOverlapException){
            return ResponseEntity.status(HttpErrorStatus.DATE_OVERLAP.value())
                    .body("Event dates overlap for account with "
                            + (exception.getMessage() !=null ? "id = " + exception.getMessage() : "unknown id"));
        }
        else {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Something went wrong.");
        }
    }

    @ExceptionHandler(EmptyAccountIdsException.class)
    public ResponseEntity<?> handleEmptyAccountIdsException(EmptyAccountIdsException exception){
        return ResponseEntity.badRequest().body("The request must contain at least 1 account id.");
    }

    @ExceptionHandler(InvalidAccountIdsException.class)
    public ResponseEntity<?> handleInvalidAccountIdsException(InvalidAccountIdsException exception){
        return ResponseEntity.badRequest().body("Invalid account ids parameter.");
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> defaultExceptionHandler(Exception exception){
        exception.printStackTrace();
        return new ResponseEntity<>("Something went wrong.", HttpStatus.I_AM_A_TEAPOT);
    }

}
