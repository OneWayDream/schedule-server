package ru.itis.scheduler.exceptions.handlers;

public enum HttpErrorStatus {

    EXISTING_LOGIN(452),
    DATE_OVERLAP(453);

    private final int value;

    HttpErrorStatus(int value){
        this.value = value;
    }

    public int value(){
        return value;
    }

}
