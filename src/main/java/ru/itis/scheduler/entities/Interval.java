package ru.itis.scheduler.entities;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@AllArgsConstructor
@Data
public class Interval {

    private LocalDateTime start;
    private LocalDateTime end;

}
