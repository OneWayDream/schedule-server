package ru.itis.scheduler.entities.dtos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EventDto {

    private Long id;
    private Long accountId;
    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private String description;

}
