package ru.itis.scheduler.entities.forms;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.scheduler.validation.ValidStartEndDateTimes;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ValidStartEndDateTimes(startTime = "startTime", endTime = "endTime")
public class MultipleAccountsEventForm {

    @NotNull
    private List<Long> accountIds;
    @NotNull
    private LocalDateTime startTime;
    @NotNull
    private LocalDateTime endTime;
    @Size(max = 50)
    private String description;

}
