package ru.itis.scheduler.entities.forms;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class NewAccountForm {

    @Size(min = 5, max = 30)
    @NotNull
    private String login;

    @Size(min = 2, max = 30)
    @NotNull
    private String name;

}
