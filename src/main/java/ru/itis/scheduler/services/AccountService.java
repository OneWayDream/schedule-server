package ru.itis.scheduler.services;

import ru.itis.scheduler.models.Account;

public interface AccountService {

    Account add(Account accountDto);
    Account findByLogin(String login);

}
