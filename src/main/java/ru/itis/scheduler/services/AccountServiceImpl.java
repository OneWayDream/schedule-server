package ru.itis.scheduler.services;

import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.scheduler.exceptions.persistence.LoginAlreadyInUseException;
import ru.itis.scheduler.models.Account;
import ru.itis.scheduler.repositories.AccountRepository;

@Service
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService {

    private final AccountRepository repository;

    @Override
    public Account add(Account account) {
        try{
            findByLogin(account.getLogin());
            throw new LoginAlreadyInUseException();
        } catch (EntityNotFoundException exception){
            //ignore
        }
        return repository.save(account);
    }

    @Override
    public Account findByLogin(String login) {
        return repository.findAccountByLogin(login).orElseThrow(EntityNotFoundException::new);
    }

}
