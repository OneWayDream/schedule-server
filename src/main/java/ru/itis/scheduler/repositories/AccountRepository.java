package ru.itis.scheduler.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.scheduler.models.Account;

import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long> {

    Optional<Account> findAccountByLogin(String login);

}
