package ru.itis.scheduler.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.scheduler.models.Event;

import java.util.List;

public interface EventRepository extends JpaRepository<Event, Long> {

    List<Event> findAllByAccountIdIn(List<Long> accountIds);

}
