package ru.itis.scheduler.validation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.springframework.beans.BeanWrapperImpl;

import java.time.LocalDateTime;

public class StartEndDateTimesValidator implements ConstraintValidator<ValidStartEndDateTimes, Object> {

    private String startTimeProperty;
    private String endTimeProperty;

    @Override
    public void initialize(ValidStartEndDateTimes constraintAnnotation) {
        this.startTimeProperty = constraintAnnotation.startTime();
        this.endTimeProperty = constraintAnnotation.endTime();
    }

    @Override
    public boolean isValid(Object object, ConstraintValidatorContext constraintValidatorContext) {
        LocalDateTime startTime = (LocalDateTime) new BeanWrapperImpl(object).getPropertyValue(startTimeProperty);
        LocalDateTime endTime = (LocalDateTime) new BeanWrapperImpl(object).getPropertyValue(endTimeProperty);
        return startTime != null && endTime != null && startTime.isBefore(endTime);
    }

}
