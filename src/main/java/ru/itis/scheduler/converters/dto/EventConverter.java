package ru.itis.scheduler.converters.dto;

import ru.itis.scheduler.entities.dtos.EventDto;
import ru.itis.scheduler.models.Event;

import java.util.List;

public class EventConverter {

    public static EventDto from(Event event){
        return EventDto.builder()
                .id(event.getId())
                .accountId(event.getAccountId())
                .startTime(event.getStartTime())
                .endTime(event.getEndTime())
                .description(event.getDescription())
                .build();
    }

    public static Event to(EventDto eventDto){
        return Event.builder()
                .id(eventDto.getId())
                .accountId(eventDto.getAccountId())
                .startTime(eventDto.getStartTime())
                .endTime(eventDto.getEndTime())
                .description(eventDto.getDescription())
                .build();
    }

    public static List<EventDto> from(List<Event> events){
        return events.stream()
                .map(EventConverter::from)
                .toList();
    }

    public static List<Event> to(List<EventDto> events){
        return events.stream()
                .map(EventConverter::to)
                .toList();
    }

}
