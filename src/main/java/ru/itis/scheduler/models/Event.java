package ru.itis.scheduler.models;

import jakarta.persistence.*;
import lombok.*;
import ru.itis.scheduler.converters.jpa.EventLocalDateTimeConverter;

import java.time.LocalDateTime;
import java.util.Objects;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@ToString
@Entity
@Table(name = "event")
public class Event {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "account_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    private Account account;

    @Column(name = "account_id")
    private Long accountId;

    @Column(name = "start_time", nullable = false)
    @Convert(converter = EventLocalDateTimeConverter.class)
    private LocalDateTime startTime;

    @Column(name = "end_time", nullable = false)
    @Convert(converter = EventLocalDateTimeConverter.class)
    private LocalDateTime endTime;

    @Column(name = "description")
    private String description;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Event event = (Event) o;
        return Objects.equals(id, event.id) && Objects.equals(account, event.account) && Objects.equals(accountId, event.accountId) && Objects.equals(startTime, event.startTime) && Objects.equals(endTime, event.endTime) && Objects.equals(description, event.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, account, accountId, startTime, endTime, description);
    }
}
