package ru.itis.scheduler.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.itis.scheduler.entities.Interval;
import ru.itis.scheduler.entities.dtos.EventDto;
import ru.itis.scheduler.entities.forms.MultipleAccountsEventForm;
import ru.itis.scheduler.entities.forms.SingleAccountEventForm;
import ru.itis.scheduler.exceptions.intervals.InvalidAccountIdsException;
import ru.itis.scheduler.services.EventService;

import java.util.Arrays;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("event")
public class EventController {

    private final EventService eventService;

    @Operation(summary = "Add a new event for account by account id.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success addition", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = EventDto.class))
            }),
            @ApiResponse(responseCode = "403", description = "Unexpected exception in the branch being handled"),
            @ApiResponse(responseCode = "404", description = "Account doesn't exist"),
            @ApiResponse(responseCode = "418", description = "Unexpected exception"),
            @ApiResponse(responseCode = "453", description = "The new event date overlaps existing events")
    })
    @PostMapping()
    public ResponseEntity<?> addEventForSingleAccount(@Valid @RequestBody SingleAccountEventForm form){
        return ResponseEntity.ok(eventService.addEvent(EventDto.builder()
                        .accountId(form.getAccountId())
                        .startTime(form.getStartTime())
                        .endTime(form.getEndTime())
                        .description(form.getDescription())
                        .build()));
    }

    @Operation(summary = "Add a new event for accounts by accounts id.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success addition", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = EventDto.class))
            }),
            @ApiResponse(responseCode = "403", description = "Unexpected exception in the branch being handled"),
            @ApiResponse(responseCode = "404", description = "Account doesn't exist"),
            @ApiResponse(responseCode = "418", description = "Unexpected exception"),
            @ApiResponse(responseCode = "453", description = "The new event date overlaps existing events")
    })
    @PostMapping("/multiple")
    public ResponseEntity<?> addEventForMultipleAccounts(@Valid @RequestBody MultipleAccountsEventForm form){
        return ResponseEntity.ok(eventService.addEvent(form.getAccountIds(),
                EventDto.builder()
                        .startTime(form.getStartTime())
                        .endTime(form.getEndTime())
                        .description(form.getDescription())
                        .build()
        ));
    }

    @Operation(summary = "Get common free intervals for a bunch of accounts.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success addition", content = {
                    @Content(mediaType = "application/json", array = @ArraySchema(
                            schema = @Schema(implementation = Interval.class)
                    ))
            }),
            @ApiResponse(responseCode = "403", description = "Unexpected exception in the branch being handled"),
            @ApiResponse(responseCode = "404", description = "Account doesn't exist"),
            @ApiResponse(responseCode = "418", description = "Unexpected exception"),
            @ApiResponse(responseCode = "453", description = "The new event date overlaps existing events")
    })
    @GetMapping("/free-intervals")
    public ResponseEntity<?> getFreeTimeIntervals(@RequestParam(name = "account_ids") String accountIdString){
        try{
            List<Long> accountIds = Arrays.stream(accountIdString.split(",")).map(Long::parseLong).toList();
            return ResponseEntity.ok(eventService.getCommonFreeIntervals(accountIds));
        } catch (NumberFormatException exception){
            throw new InvalidAccountIdsException();
        }
    }

}
