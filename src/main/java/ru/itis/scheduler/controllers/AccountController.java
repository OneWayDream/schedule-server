package ru.itis.scheduler.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.scheduler.entities.forms.NewAccountForm;
import ru.itis.scheduler.models.Account;
import ru.itis.scheduler.services.AccountService;

@RestController
@RequestMapping("account")
@RequiredArgsConstructor
public class AccountController {

    private final AccountService accountService;

    @Operation(summary = "Add a new account.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Success addition", content = {
                    @Content(mediaType = "application/json", schema = @Schema(implementation = Account.class))
            }),
            @ApiResponse(responseCode = "403", description = "Unexpected exception in the branch being handled"),
            @ApiResponse(responseCode = "418", description = "Unexpected exception"),
            @ApiResponse(responseCode = "452", description = "Login is already in use")
    })
    @PostMapping
    public ResponseEntity<?> createAccount(@Valid @RequestBody NewAccountForm accountForm){
        return ResponseEntity.ok(accountService.add(Account.builder()
                .login(accountForm.getLogin())
                .name(accountForm.getName())
                .build()));
    }

}
